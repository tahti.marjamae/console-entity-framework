﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEntityFramework
{
    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";        //sellises kohas programm ei kirjuta database üle, muidu muudetakse kogu databaasis asjad ära ja läheb tuksi
    }

    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();

            #region database kommenteerin välja
            ne.Database.Log = Console.WriteLine;      //trükib SQL-i välja. Ei tea selelst veel midagi
            foreach (var p in ne.Products
                .Select(x => new {x.UnitPrice, x.ProductName})
                .Where(x => x.UnitPrice > 100) //sellest asjast tehakse SQL
                .OrderBy(x => x.UnitPrice))
                Console.WriteLine($"{p.ProductName}");

            Console.WriteLine(
                ne.Categories.Find(8).CategoryName);

            //ne.Categories.Find(8).CategoryName = "Seafood";     //muutsin nime seafood => seatoit ja printisin uuesti välja
            //Console.WriteLine(
            //    ne.Categories.Find(8).CategoryName);

            //ne.SaveChanges();       //nüüd salvestab seatoit uueks nimeks ka andmebaasis

            //Product uus = new Product { ProductName = "Mängukaru", UnitPrice = 10, CategoryID = 8 };
            //ne.Products.Add(uus);
            //ne.SaveChanges();

            //ne.Categories.Find(1).Products.Add(new Product { ProductName = "Lutsukomm", UnitPrice = 0.5M });    //lisab kategooriasse nr 1 uue toote lutsukomm. 
            //ne.SaveChanges();

            //foreach (var p in ne.Products.Where(x => x.ProductName.StartsWith("Mängu")))ne.Products.Remove(p);  //kustutab databaasist kõik mängukarud
            //ne.SaveChanges();
            #endregion

            foreach (var e in ne.Employees) Console.WriteLine($"{e.FullName} ülemus on {e.Manager?.FullName??"ta on ise jumal"}");
        }
    }
}
